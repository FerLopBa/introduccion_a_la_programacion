Inicio

    Variable Lado es Numérico Real
    Variable Perimetro es Numérico Real

    Lado = 20
    Perimetro = 0

    Leer "Escriba el la longitud de los lados del triángulo equilátero", dolares

    Si (0 < Lado)
	    Perimetro = Lado * 3
	Sino
	    Imprimir "Los lados tienen que ser un número positivo"
    Fin-Si
    Imprimir Perimetro
Fin