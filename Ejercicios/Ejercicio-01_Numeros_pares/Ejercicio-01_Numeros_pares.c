// Programa de n�meros pares, programa que imprime los n�meros pares del 0 al 100

#include <stdio.h>
#include <math.h>

int main()
{
	int i;
	for(i = 0; i <= 100 ; i += 2){
		printf("%i\n", i);
	}
	return 0;
}
